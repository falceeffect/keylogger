#ifndef INC_FTPLISTPARSE_H
#define INC_FTPLISTPARSE_H

#include <string>
#include "Definements.h"
#include "FTPClient.h"

namespace nsFTP
{

class CFTPFileStatus;

/// Implements the parsing of the string returned by the LIST command.
class CFTPListParser : public CFTPClient::IFileListParser
{
public:
   CFTPListParser();
   virtual ~CFTPListParser();

public:
   bool Parse(CFTPFileStatus& ftpFileStatus, const tstring& strLineToParse);

private:
   bool IsEPLS(LPCTSTR pszLine);
   bool ParseEPLF(CFTPFileStatus& ftpFileStatus, LPCTSTR pszLine, int iLength);

   bool IsUNIXStyleListing(LPCTSTR pszLine);
   bool ParseUNIXStyleListing(CFTPFileStatus& ftpFileStatus, LPCTSTR pszLine, int iLength);

   bool IsMultiNetListing(LPCTSTR pszLine);
   bool ParseMultiNetListing(CFTPFileStatus& ftpFileStatus, LPCTSTR pszLine, int iLength);

   bool IsMSDOSListing(LPCTSTR pszLine);
   bool ParseMSDOSListing(CFTPFileStatus& ftpFileStatus, LPCTSTR pszLine, int iLength);

   bool CheckMonth(LPCTSTR pszBuffer, LPCTSTR pszMonthName) const;
   int  GetMonth(LPCTSTR pszBuffer, int iLength) const;
   bool GetLong(LPCTSTR pszLong, int iLength, long& lResult) const;
   long GetYear(time_t time) const;

   long ToTAI(long lYear, long lMonth, long lMDay) const;
   long GuessTAI(long lMonth, long lMDay);

private:
   time_t        m_tmBase;       // time() value on this OS at the beginning of 1970 TAI
   long          m_lCurrentYear; // approximation to current year
   static const TCHAR* m_Months[12];
};

}

#endif // INC_FTPLISTPARSE_H
