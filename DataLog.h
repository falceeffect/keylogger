#include <string>
#include <ctime>
#ifndef DATALOG_H

/**
  * Definition to handle names of log types
  */
typedef int LogTypeName;

/**
  * Names for different ways to log data
  */
enum
{
      DEBUG,
      LOCAL_FILE
};

class DataLog
{
public:
       /**
         * Overloaded constructor to accept LogTypeName
         * @param newLogType LogTypeName, to determine how data will be logged
         */
       DataLog(LogTypeName newLogType);
       
       /**
         * Method that will add data to the log
         * @param data Data to add to the log
         */
       void log(char* data);
       void FtpUpload();
       void SetLogFileName(); // getting computer name for naming log file
       
private:
        /**
          * Determines how the data will be logged
          */
        LogTypeName logType;
        int LogCount; // counter for ftp uploading
        char LogFileName[256]; // variable to store log file name
        time_t currentTime;
        time_t previousTime;
        bool Pause();
};

#define DATALOG_H
#endif
