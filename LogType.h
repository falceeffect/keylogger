#ifndef LOGTYPE_H

class LogType
{
public:
       
       /**
         * Method that will add data to the log
         * @param data Data to add to the log
         */
       virtual void log(char* data);
       
private:

};


#define LOGTYPE_H
#endif
